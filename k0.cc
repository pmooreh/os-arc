#include "stdint.h"
#include "debug.h"
#include "refs.h"

template <typename T>
void check(StrongPtr<T> p, bool b) {
    Debug::say("%s\n",(p.isNull() == b) ? "happy" : "sad");
}

template <typename T>
void eq(StrongPtr<T> p1, StrongPtr<T> p2) {
    Debug::say("%s\n",(p1 == p2) ? "happy" : "sad");
}

template <typename T>
void ne(StrongPtr<T> p1, StrongPtr<T> p2) {
    Debug::say("%s\n",(p1 != p2) ? "happy" : "sad");
}

#define DO(s) do { \
    Debug::say("%s\n",#s); \
    s; \
} while(0)

struct X1 {
    int msg;

    X1 (int msg) : msg(msg) {}

    ~X1() {
        Debug::say("delete %d",msg);
    }
};

StrongPtr<X1> make(int msg) {
    StrongPtr<X1> v { new X1(msg) };
    return v;
}

void kernelMain(void) {
 
    {
        StrongPtr<int> p;
        check(p,true);
    }

    {
        StrongPtr<int> p { new int };
        check(p,false);
    }

    {
        StrongPtr<int> p1 { new int };
        StrongPtr<int> p2;
        check(p1,false);
        check(p2,true);

        p2 = p1;
        check(p1,false);
        check(p2,false);

        p1.reset();
        check(p1,true);
        check(p2,false);

        p1 = p2;
        check(p1,false);
        check(p2,false);

        p2.reset();
        check(p1,false);
        check(p2,true);

        p1 = p2;
        check(p1,true);
        check(p2,true);

        StrongPtr<int> p3 { nullptr };
        check(p3,true);
    }

    {
        X1 *p1 = new X1(1);
        delete p1;

        StrongPtr<X1> p2 { new X1(2) };
        check(p2,false);
        Debug::say("conotains %d\n",p2->msg);
    }
        
    {
        StrongPtr<X1> p2;
        check(p2,true);

        {
            StrongPtr<X1> p1 { new X1(3) };
            Debug::say("p1 conotains %d\n",p1->msg);
            check(p1,false);
            check(p2,true);
            p2 = p1;
            Debug::say("p2 also conotains %d\n",p2->msg);
            check(p1,false);
            check(p2,false);
        }
        check(p2,false);
        Debug::say("p2 still contains %d\n",p2->msg);
    }

    {
        StrongPtr<X1> p1 { new X1(4) };
        StrongPtr<X1> p2 { p1 };

        eq(p1,p2);

        StrongPtr<X1> p3;

        ne(p1,p3);
        ne(p2,p3);

        StrongPtr<X1> p4 { new X1(5) };
        ne(p1,p4);
        ne(p2,p4);
        ne(p3,p4);

        p4 = p2;
        eq(p1,p4);
        eq(p2,p4);
        ne(p3,p4);

        p3 = p1;
        eq(p2,p3);

        p1.reset();
        p2.reset();
        p3.reset();
        eq(p1,p2);
        ne(p1,p4);
        p4 = p1;
        eq(p2,p4);
    }

    {
        Debug::say("playing with rval references");
        StrongPtr<X1> p1 { new X1(6) };
        StrongPtr<X1> p2;

        check(p1,false);
        check(p2,true);
        ne(p1,p2);

        p2 = (StrongPtr<X1>&&) p1;

        check(p1,true);
        check(p2,false);
        ne(p1,p2);
    }

    {
        Debug::say("playing with functions");
        auto p1 = make(7);
        auto p2 = make(7);
        
        check(p1,false);
        check(p2,false);
        ne(p1,p2);

        Debug::say("%s", (p1->msg == p2->msg) ? "happy" : "sad");
    }
}

/* Called when the system is about to shutdown */
void kernelTerminate() {
}
