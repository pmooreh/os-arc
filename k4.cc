#include "debug.h"
#include "thread.h"
#include "machine.h"

int ran = 0;
void run(void) {
    getThenIncrement(&ran,1);
}

void kernelMain(void) {
    {
        for (int i=0; i<10000; i++) {
            threadCreate(run);
        }
    }
}

void kernelTerminate() {
    Debug::say("ran:%d",ran);
    Debug::say("nCreate:%d",Thread::nCreate);
    Debug::say("nStart:%d",Thread::nStart);
    Debug::say("nDone:%d",Thread::nDelete);
}
