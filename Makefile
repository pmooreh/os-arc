default : compile0;

test : test0 test1 test2 test3 test4 test5 test6;

compile% :
	rm -f kernel/kernel*
	ln k$*.cc kernel/kernel.cc
	(make -C kernel kernel.img)

test% : compile%
	-@echo -n "$@ ... "
	expect e0.tcl > k$*.raw
	-@egrep '^\*\*\*' k$*.raw > k$*.out 2>&1
	-@((diff -b k$*.out k$*.ok > k$*.diff 2>&1) && echo "pass") || echo "failed, look at k$*.raw, k$*.ok, k$*.out, and k$*.diff for more information"
	
% :
	(make -C kernel $@)

clean:
	-(make -C kernel clean)
