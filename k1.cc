#include "stdint.h"
#include "debug.h"
#include "thread.h"
#include "locks.h"
#include "refs.h"

volatile bool done = false;

void simple(const char* msg) {
    Debug::say(msg);
    done = true;
};

void kernelMain(void) {
 
    /* wait for flag */
    Debug::say("wait using loop");
    threadCreate(simple,"h0");
    while (!done);

    /* wait for exitEvent */
    Debug::say("wait using exitEvent");
    threadCreate(simple,"h1")->exitEvent->wait();
    
    /* wait for deleteEvent */
    {
        Debug::say("wait using deleteEvent");
        StrongPtr<Thread> t = threadCreate(simple,"h2");
        t->exitEvent->wait();
        Debug::say("h2 exit");
        StrongPtr<Event> e = t->deleteEvent;
        t.reset(); // release your reference
        Debug::say("waiting for delete");
        e->wait();
        Debug::say("h2 delete");
    }
    
    {
        StrongPtr<Event> e;
        {
            Debug::say("h3 wait using deleteEvent");
            auto t = threadCreate(simple,"h3");
            t->exitEvent->wait();
            Debug::say("h3 exit");
            e = t->deleteEvent;
            Debug::say("h3 going out of scope");
        }
        Debug::say("h3 waiting for delete");
        e->wait();
        Debug::say("h3 delete");
    }

}

/* Called when the system is about to shutdown */
void kernelTerminate() {
    Debug::say("nCreate:%d",Thread::nCreate);
    Debug::say("nStart:%d",Thread::nStart);
    Debug::say("nDelete:%d",Thread::nDelete);
}
