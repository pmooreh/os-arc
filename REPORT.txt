- What's the point of having r-value references?

- The Thread class has a strange arrangement; it contains a
  StrongPtr to itself called me. Why is it there?

   class Thread {
        ...
        StrongPtr<Thread> me;

   };


   ptr->me = StrongPtr<Thread> { ptr };

   Why is it there? What happens if we remove it?


- What's wrong with the following code?

    Event* ptr = new Event();
    StrongPtr<Event> p1 { ptr };
    StrongPtr<event> p2 { ptr };

what's the correct way?
