#include "stdint.h"
#include "debug.h"
#include "thread.h"
#include "machine.h"
#include "locks.h"
#include "refs.h"

int ran = 0;
void doit(void) {
    ran += 1;
};

void kernelMain(void) {
    {
        for (int i=0; i<100; i++) {
            for (int j=0; j<100; j++) {
                auto t = threadCreate(doit);
                auto e = t->deleteEvent;
                t.reset();
                e->wait();
            }
        }
    }
}

void kernelTerminate() {
    Debug::say("ran:%d",ran);
    Debug::say("nCreate:%d",Thread::nCreate);
    Debug::say("nStart:%d",Thread::nStart);
    Debug::say("nDone:%d",Thread::nDelete);
}
