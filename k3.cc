#include "stdint.h"
#include "debug.h"
#include "idt.h"
#include "pic.h"
#include "pit.h"
#include "thread.h"
#include "machine.h"
#include "heap.h"
#include "random.h"
#include "config.h"
#include "u8250.h"
#include "locks.h"
#include "refs.h"

#define N 100

int count = 0;

class doit : public Thread {
    int index;
    StrongPtr<BoundedBuffer<StrongPtr<Barrier>>> buffer;
public:
    doit(int index, StrongPtr<BoundedBuffer<StrongPtr<Barrier>>> buffer) : index(index), buffer(buffer) {}

    void run() {
        if (index == N/2) {
            Debug::say("creating barrier");
            StrongPtr<Barrier> b { new Barrier(N+1) };
            for (int i=0; i<=N; i++) {
                buffer->put(b);
            }
        }
        StrongPtr<Barrier> b { buffer->get() };
        getThenIncrement(&count,1);
        b->sync();
    }
};

void kernelMain(void) {
    StrongPtr<BoundedBuffer<StrongPtr<Barrier>>> buffer { new BoundedBuffer<StrongPtr<Barrier>>(1) };

    for (int i=0; i<N; i++) {
        Thread::start(new doit(i,buffer));
    }

    StrongPtr<Barrier> b = buffer->get();
    b->sync();

    Debug::say("count = %d",count);
}

void kernelTerminate() {
    Debug::say("nCreate:%d",Thread::nCreate);
    Debug::say("nStart:%d",Thread::nStart);
    Debug::say("nDone:%d",Thread::nDelete);
}

