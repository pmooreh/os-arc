#ifndef _MACHINE_H_
#define _MACHINE_H_

#include "stdint.h"

extern "C" int getThenIncrement(volatile int* ptr, int d);

extern "C" uint32_t cli(void);
extern "C" uint32_t sti(void);

extern "C" int inb(int port);
extern "C" int inl(int port);
extern "C" void outb(int port, int val);

extern "C" void pit_do_init(uint32_t d);

extern "C" void ltr(uint32_t tr);

extern "C" void pageFaultHandler();

extern "C" uint32_t getcr0();
extern "C" uint32_t getcr3();
extern "C" void invlpg(uint32_t);

extern "C" void pitAsmHandler(void);

#endif
