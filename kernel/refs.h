#ifndef _REFS_H_
#define _REFS_H_

#include "machine.h"
#include "debug.h"


class RefCount {
	// maintains count of references
	int count;
public:
	RefCount() : count(0) {}

	RefCount(int n) : count(n) {}

	int getCount() {
		return count;
	}
	
	int decRefCount() {
		int result = getThenIncrement(&count, -1) - 1;
		return result;
	}

	int incRefCount() {
		int result = getThenIncrement(&count, 1) + 1;
		return result;
	}
};


template <class T>
class StrongPtr {

// state variables: has a reference to the object, a reference to the count
T* nakedPointer;
RefCount* refCount;

public:
    /* construct a null reference */
    StrongPtr() : nakedPointer(nullptr), refCount(nullptr) {
        //Debug::printf("Constructor: null reference\n");
    }

    /* construct a reference to the given object, must handle nullptr */
    explicit StrongPtr(T* ptr) : nakedPointer(ptr), refCount(new RefCount(1)) {
        //Debug::printf("Constructor: raw pointer\n");
    }

    /* reference the same object referenced by src */
    StrongPtr(const StrongPtr& src) {
        if (src.nakedPointer != nullptr) {
            // give exact same values as src, then increment the count
            nakedPointer = src.nakedPointer;
    		refCount = src.refCount;
    		refCount->incRefCount();
        }
        else {
            // set all to nullptr
            refCount = nullptr;
            nakedPointer = nullptr;
        }

        //Debug::printf("Constructor: other StrongPtr\n");
        //Debug::printf("    points to:%d\n", (int)src.nakedPointer);
    }

    /* steal the reference from src */
    StrongPtr(StrongPtr&& src) {
        // same as above, but then reset src
        nakedPointer = src.nakedPointer;
		refCount = src.refCount;
        refCount->incRefCount();

        src.reset();

        //Debug::printf("Constructor: stolen StrongPtr\n");
    }

    /* remove one reference */
    ~StrongPtr() {
		reset();
    }

    /* return the refernced object (or nullptr) */
    T* operator -> () {
        return nakedPointer;
    }

    /* check if the reference object is nullptr */
    bool isNull() {
        return nakedPointer == nullptr;
    }

    /* set the reference to null */
    void reset() {
        int remaining = refCount->decRefCount();
        if (remaining == 0) {
            delete refCount;
            delete nakedPointer;
        }

        refCount = nullptr;
        nakedPointer = nullptr;
    }

    /* assignment, copy the src */
    StrongPtr<T>& operator = (const StrongPtr& src) {
        // first, clean up the state
        reset();

        // now just copy stuff
        nakedPointer = src.nakedPointer;
        refCount = src.refCount;

        // increment the count
        refCount->incRefCount();

        return *this;
    }

    /* assignment, steal the src */
    StrongPtr<T>& operator = (StrongPtr&& src) {
        // first, clean up the state
        reset();

        // now just copy stuff
        nakedPointer = src.nakedPointer;
        refCount = src.refCount;

        // increment the count
        refCount->incRefCount();

        src.reset();

        return *this;
    }

    /* do this and other reference the same object */
    bool operator ==(const StrongPtr<T>& other) {
        // just check reference?
        return nakedPointer == other.nakedPointer;
    }

    /* do this and other reference different objects */
    bool operator !=(const StrongPtr<T>& other) {

        return nakedPointer != other.nakedPointer;
    }
};

#endif
