#include "debug.h"
#include "idt.h"
#include "pic.h"
#include "pit.h"
#include "thread.h"
#include "heap.h"
#include "random.h"
#include "config.h"
#include "u8250.h"
#include "alarm.h"
#include "init.h"

Config config;

extern "C" void kernelInit(void) {
    U8250 uart;
    Debug::init(&uart);
    Debug::debugAll = false;
    Debug::printf("\nHello, is there anybody out there?\n");

    /* discover configuration */
    configInit(&config);

    /* Initialize heap */
    heapInit((void*)0x100000,0x300000);

    /* initialize random number generator */
    randomInit(0);

    /* initialize threads */
    Thread::init();

    /* initialize idt */
    idtInit();

    /* initialize pic */
    picInit();

    /* initialize pit */
    Pit::init(10000);

    /* initialize alarms */
    Alarm::init();

    /* enable interrupts */
    picEnable();

    kernelMain();

    Thread::shutdown();

    Thread::exit();

    Debug::panic("should never see this");
}
