#include "alarm.h"
#include "pit.h"
#include "locks.h"
#include "thread.h"
#include "debug.h"

void alarmThread(void) {
    while (Thread::alive) Thread::yield();
}

void Alarm::init() {
    threadCreate(alarmThread);
    //MISSING();
}

void Alarm::schedule(uint32_t after, StrongPtr<Event> event) {
    MISSING();
}