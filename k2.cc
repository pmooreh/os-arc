#include "stdint.h"
#include "debug.h"
#include "idt.h"
#include "pic.h"
#include "pit.h"
#include "thread.h"
#include "machine.h"
#include "heap.h"
#include "random.h"
#include "config.h"
#include "u8250.h"
#include "locks.h"
#include "refs.h"

#define M 4

int mCount = 0;
int fCount = 0;

class f0 : public Thread {
    int* p;
    StrongPtr<Barrier> done;

public:
  f0(int* p, StrongPtr<Barrier> done) : p(p), done(done) {}

  virtual void run() {
    //Debug::printf("start %d\n",*p);

    void* last[M];

    for (int i=0; i<M; i++) {
        last[i] = 0;
    }

    for (int i=0; i<*p; i++) {
        int x = random() % M;
        if (last[x] != 0) {
            free(last[x]);
            getThenIncrement(&fCount,1);
        }
        size_t sz = random() % 1000;
        last[x] = malloc(sz);
        getThenIncrement(&mCount,1);
        if (last[x] == 0) {
            Debug::say("failed to allocate %d",sz);
        }
    }

    //Debug::printf("checking %d\n",*p);

    for (int i=0; i<M; i++) {
        if (last[i] != 0) {
            free(last[i]);
            getThenIncrement(&fCount,1);
        }
    }

    //Debug::printf("finished %d\n",*p);

    *p = 0;
    done->sync();
  }
};

#define N 100

int status[N];
    
void one(const char* name) {
    Debug::say("%s",name);
    int count = 0;
    StrongPtr<Barrier> done { new Barrier(N) };

    for (int i=1; i<N; i++) {
        status[i] = i * 100;
        Thread::start(new f0(&status[i],done));
        count++;
    }

    Debug::say("created %d",count);

    done->sync();

    Debug::say("checking output");
    for (int i=1; i<N; i++) {
        if (status[i] != 0) {
            Debug::say("thread failed %d, status[%d] = %d",i,i,status[i]);
        }
        count --;
    }

    Debug::say("final count %d",count);

    //delete done;
    //done = nullptr;
}

class b0 : public Thread {
    const char* arg;
    StrongPtr<Barrier> done;
public:
    b0(const char *arg, StrongPtr<Barrier> done) : arg(arg), done(done) {}

    void run() {
        if (arg != nullptr) Debug::say(arg);
        done->sync();
    }
};

void semUp(StrongPtr<Semaphore> sem) {
    Debug::say("saying up");
    sem->up();
}

class pingPong : public Thread {
    StrongPtr<Semaphore> sem1;
    StrongPtr<Semaphore> sem2;
public:
    pingPong(StrongPtr<Semaphore> sem1, StrongPtr<Semaphore> sem2) :
         sem1(sem1), sem2(sem2) {}

    virtual void run() {
        sem1->down();
        Debug::say("3");
        sem2->up();
    }
};

class chain : public Thread {
  StrongPtr<Semaphore> me;
  StrongPtr<Semaphore> prev;
  int i;
public:
  chain(StrongPtr<Semaphore> me, StrongPtr<Semaphore> prev, int i) :
     me(me), prev(prev), i(i) {}

  virtual void run() {
    me->down();
    Debug::say("hello from %d",i);
    prev->up();
  }
};

class eventChain : public Thread {
  StrongPtr<Event> me;
  StrongPtr<Event> prev;
  int i;
public:
  eventChain(StrongPtr<Event> me, StrongPtr<Event> prev, int i) :
     me(me), prev(prev), i(i) {}

  virtual void run() {
    me->wait();
    Debug::say("E%d",i);
    prev->signal();
  }
};

class lockTest : public Thread {
    StrongPtr<BlockingLock> lock0;
    StrongPtr<BlockingLock> lock1;
public:
    lockTest(StrongPtr<BlockingLock> lock0, StrongPtr<BlockingLock> lock1) :
        lock0(lock0), lock1(lock1)
    {
    }

    void run() {
        lock0->lock();
        Debug::say("L2");
        lock1->unlock();
    }
};

////////////

class bufferTest : public Thread {
    StrongPtr<BoundedBuffer<int>> in;
    StrongPtr<BoundedBuffer<int>> out;
public:
    bufferTest(StrongPtr<BoundedBuffer<int>> in, StrongPtr<BoundedBuffer<int>> out) :
        in(in), out(out)
    {
    }

    void run() {
        int x = 0;
        while (true) {
            int v = out->get();
            if (v == -1) break;
            for (int i=0; i<v; i++) {
                void* p = malloc(i);
                free(p);
                x++;
            }
        }
        Debug::say("F9");
        Debug::say("F8");
        in->put(x);
    }
};

void simple(const char* msg) {
    Debug::say(msg);
};

void kernelMain(void) {

    {
        StrongPtr<Thread> t = threadCreate(simple,"hello");
        t->exitEvent->wait();
    }

    ////////////////
    // Semaphores //
    ////////////////

    Debug::say("testing semaphores");
    {
        StrongPtr<Semaphore> sem { new Semaphore(1) };
        sem->down();
        Debug::say("down works");
        Debug::say("creating a thread to say up");
        threadCreate(semUp,sem);
        sem->down();
        Debug::say("up worked");
    }

    {
        StrongPtr<Semaphore> sem1 { new Semaphore(0) };
        StrongPtr<Semaphore> sem2 { new Semaphore(0) };

        Debug::say("1");
        Thread::start(new pingPong(sem1,sem2));
        Debug::say("2");
        sem1->up();
        sem2->down();
        Debug::say("4");
    }

    {
      StrongPtr<Semaphore> sems[10];
      for (int i=0; i<10; i++) {
        sems[i] = StrongPtr<Semaphore> { new Semaphore(0) };
      }
      for (int i=1; i<10; i++) {
        Thread::start(new chain(sems[i],sems[i-1],i));
      }
      sems[9]->up();
      sems[0]->down();
      Debug::say("chain is looking good");
    }


    ////////////
    // Events //
    ////////////
        
    {
        StrongPtr<Event> events[10];
        for (int i=0; i<10; i++) {
            events[i] = StrongPtr<Event> { new Event() };
        }
        for (int i=1; i<10; i++) {
            Thread::start(new eventChain(events[i],events[i-1],i));
        }
        Debug::say("E10");
        events[9]->signal();
        events[0]->wait();
        Debug::say("E0");
        events[0]->wait();
        Debug::say("E0");
    }

    //////////
    // Lock //
    //////////

    {
        StrongPtr<BlockingLock> lock0 { new BlockingLock() };
        StrongPtr<BlockingLock> lock1 { new BlockingLock() };

        lock0->lock();
        lock1->lock();
        Thread::start(new lockTest(lock0,lock1));
        Debug::say("L1");
        lock0->unlock();
        lock1->lock();
        Debug::say("L3");
    }

    
    /////////////
    // Barrier //
    /////////////

    {
        Debug::say("test barriers");
        StrongPtr<Barrier> done { new Barrier(2) };
        Thread::start(new b0("B0",done));
        done->sync();
        Debug::say("B1");
    }

    {
        Debug::say("Barrier(10)");
        StrongPtr<Barrier> done { new Barrier(10) };
        for (int i=1; i<10; i++) {
            Thread::start(new b0(nullptr,done));
        }
        done->sync();
        Debug::say("back");
    }


    ///////////////////
    // BoundedBuffer //
    ///////////////////

    {
        StrongPtr<BoundedBuffer<int>> out { new BoundedBuffer<int>(4) };
        StrongPtr<BoundedBuffer<int>> in { new BoundedBuffer<int>(1) };

        Debug::say("F1");
        out->put(10);
        Debug::say("F2");
        out->put(20);
        Debug::say("F3");
        out->put(30);
        Debug::say("F4");
        out->put(40);
        Thread::start(new bufferTest(in,out));
        Debug::say("F5");
        out->put(50);
        Debug::say("F6");
        out->put(-1);
        Debug::say("%d",in->get());
        Debug::say("F10");
    }

    ///////////
    // Noise //
    ///////////

    Debug::say("going for the big one");
    one("---- phase1 ----");
    Debug::say("going for the big one, again");
    one("---- phase2 ----");

    if (mCount != fCount) {
        Debug::say("mCount %d",mCount);
        Debug::say("fCount %d",fCount);
    } else {
        Debug::say("counts match");
    }

    if (mCount < 500000) {
        Debug::say("mCount is too small %d",mCount);
    } else {
        Debug::say("mCount >= 500000");
    }

}

void kernelTerminate() {
    Debug::say("nCreate:%d",Thread::nCreate);
    Debug::say("nStart:%d",Thread::nStart);
    Debug::say("nDone:%d",Thread::nDelete);
}
