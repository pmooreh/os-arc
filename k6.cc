#include "stdint.h"
#include "debug.h"
#include "thread.h"
#include "locks.h"
#include "refs.h"
#include "pit.h"
#include "alarm.h"

volatile uint32_t finish = 0;

class Handler : public Thread {
    uint32_t start;
    StrongPtr<Event> event;
public:
    Handler(uint32_t start, StrongPtr<Event> event) : start(start), event(event) {}

    void run() {
        event->wait();
        finish = Pit::ticks;
    }
};

void check(uint32_t s, uint32_t f) {
    uint32_t delta = f - s;
    uint32_t oneSec = Pit::secondsToTicks(1);

    if ((delta < oneSec) || (delta > 3*oneSec)) {
        Debug::say("out of range, delta:%d start:%d finish:%d oneSec:%d",delta,s,f,oneSec);
    } else {
        Debug::say("in range");
    }
}

void kernelMain(void) {
    {
        StrongPtr<Event> e { new Event() };
        uint32_t start = Pit::ticks;

        Alarm::schedule(1,e);

        e->wait();
        check(start,Pit::ticks);
    }
    {
        StrongPtr<Event> e { new Event() };
        uint32_t start = Pit::ticks;

        auto handler = Thread::start(new Handler(start,e));

        Alarm::schedule(1,e);

        handler->exitEvent->wait();
        check(start,finish);
    }

}

void kernelTerminate() {
    Debug::say("nCreate:%d",Thread::nCreate);
    Debug::say("nStart:%d",Thread::nStart);
    Debug::say("nDone:%d",Thread::nDelete);
}
